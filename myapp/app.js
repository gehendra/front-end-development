const port = 8000;
/*
const http = require('http');



const server = http.createServer(function(req, res) {
  res.status = 200;
  res.setHeader('Content-Type','text/plain');
  res.end('Welcome Gehendra to node. This is your first app. Lets see if nodemon is working.');
});

server.listen(port, function(){
  console.log('Server is running..');
});
*/

var express = require('express');

var app = express();

app.get('/', function(req, res){
  res.send('Hello World');
});

app.listen(port, function(){
  console.log(`Server running at ${port}`);
});
